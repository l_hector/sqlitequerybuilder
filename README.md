# SqliteQueryBuilder

Sqlite builder

## Como usar para hacer consultas
* Select con *n* columnas 
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();

     builder.from("TABLA_NAME")
        .select("columna1", "columna2", "columna3")
        .where("value1", [operador], "value2");
    
    String query = builder.toString();
...
```

Código resultante:
``` sql
    SELECT 'columna1', 'columna2', 'columna3' 
    FROM 'TABLA_NAME'
```
Ademas o admite String[] para las columnas, se puede agregar columnas  después de ser creado el builder.
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .select(columas);
    builder.select("columna3").select("columna4");
    builder.select("columna5");

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'columna1', 'columna2', 'columna3', 'columna4' ,'columna5' 
    FROM 'TABLA_NAME'
```

Consultas con formato Tabla.columa y *alias*
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .selectOnTable("Tabla1", columnas)
        .selectAs("columan3", "alias1")

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'Tabla1.columna1', 'Tabla1.columna2', 'columna3' AS alias1
    FROM 'TABLA_NAME'
```
Consultas con funciones agregadas SUM, MAX, AVG, etc.
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .select(columnas)
        .selectWithFunc(AggregateFuncions.SUM, "columna3", "total");

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'columna1', 'columna2', SUM('columna3') AS total
    FROM 'TABLA_NAME'
```

**Nota:** la clase AggregateFuncions.java contiene todas las fuciones agregadas disponibles.

### Condiciones WHERE 
**Operadores són todos aquellos naturales de SQlite:**
* =         igual
* < ó >     mayor o menor
* '>= ó <=  igual o mayor, igual o menor
* <>        distinto
* LIKE      like

**Métodos where permetidos**
```java
...
    .where(key, operador, value);
    .orWhere(key, operador, valye)
    .whereIn(key, values..)
    .orWhereIn(key, values..)
    .whereNull(column)
    .orWhereNull(column)
    .whereNotNull(column)
    .orWhereNotNull(column)
...
```
Ejemplo: 
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .select(columnas)
        .where("columna1", "=", "algo")
        .where("columna2", "", 25)
        .orWhere("columna3", "=", "P");

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'columna1', 'columna2'
    FROM 'TABLA_NAME'
    WHERE columa3 = 'algo' AND columna2 > 25 OR columna3 = 'P'
```
Existe un método que permite agregar código raw dentro del where para validaciones muy especificas mas no es muy recomendable 
Ejemplo: 
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .select(columnas)
        .whereNull("columna1")
        .whereRaw(" (columna2 > 150 and columna3 like '%usuario%') ");

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'columna1', 'columna2'
    FROM 'TABLA_NAME'
    WHERE columa3 IS NULL AND (columna2 > 150 and columna3 like '%usuario%')
```
### Consultas con Join 
Para hacer consultas con join se tienen los siguientes métodos tanto para WHERE como para JOIN:

**Métodos join**
```java
...
   .innerJoin(table, on)
   .leftJoin(table, on)
   .rightJoin(table, on)
...
```
Ejemplo:
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .selectOnTable("TABLA_NAME", columnas)
        .selectOnTable("tabla2", "columa1", "columna2")
        .selectOnTable("tabla3", "columa1", "columna2")
        .innerJoin("tabla2", "TABLA_NAME.id = tabla2.id")
        .leftJoin("tabla3", "tabla3.id = tabla2.id")
        .where("tabla3.columna4", "=", "P" );

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'TABLA_NAME.columna1', 'TABLA_NAME.columna2', "tabla2.columna1",
            "tabla2.columna2", "tabla3.columna1", "tabla3.columna2" 
    FROM 'TABLA_NAME'
        INNER JOIN tabla2 ON TABLA_NAME.id = tabla2.id)
        LEFT JOIN tabla3 ON tabla3.id = tabla2.id)
    WHERE tabla3.columna4 = 'P'
```
### Otras fuciones de SQlite 
**Métodos**
```java
...
    .disting()
    .groupBy(columna)
    .groupBy(tabla, columna)
    .orderBy(columnas...)
    .ascending()
    .descending()
...
```
Ejemplo:
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"}

    builder.from("TABLA_NAME")
        .selectOnTable("TABLA_NAME", columnas)
        .innerJoin("tabla2", "TABLA_NAME.id = tabla2.id")
        .where("tabla2.columna4", "=", "P" );
        .orderBy("TABLA_NAME.columna1")
        .ascending();
    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'TABLA_NAME.columna1', 'TABLA_NAME.columna2'
    FROM 'TABLA_NAME'
        INNER JOIN tabla2 ON TABLA_NAME.id = tabla2.id)
    WHERE tabla3.columna4 = 'P'
    ORDER BY TABLA_NAME.columna1 asc
```
### Para condiciones mas complejas utlizar **SimpleWhere.java**
SimpleWhere permite crear condiciones where anidadas mas complejeas de dos o mas condiciones.

Ejemplo:
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    String[] columnas = {"columna1", "columna2"};
    SimpleWhere simpleWhere = new SimpleWhere(true);

    simpleWhere.where("columna2", ">" 150);
    simpleWhere.where("columna3", "LIKE" "usuario");

    builder.from("TABLA_NAME")
        .select(columnas)
        .whereNull("column3")
        .whereRaw(simpleWhere.toStringCompact());

    String query = builder.toString();
...
```
Código resultante:
``` sql
    SELECT 'columna1', 'columna2' 
    FROM 'TABLA_NAME'
        INNER JOIN tabla2 ON TABLA_NAME.id = tabla2.id)
        LEFT JOIN tabla3 ON tabla3.id = tabla2.id)
    WHERE columa3 IS NULL AND (columna2 > 150 and columna3 like '%usuario%')
```

**Nota:** simpleWhere.***toStringCompact();*** agrega parentisis a la condición  "(" + [where] + ")"

### Consultas (aún en desarrollo)
Después de crear el querybuilder se puede acceder al metodo find find() que refresará un Cursor de la consulta

Ejemplo:
```java
...
    TelynetQuery.Builder builder = new TelynetQuery.Builder();
    //TODO sqlbilder
    Cursor cursos = builder.createReadOnly().find();
...
```