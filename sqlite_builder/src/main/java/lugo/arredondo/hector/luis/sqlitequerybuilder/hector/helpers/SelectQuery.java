package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Hector Arredondo on 01/06/2018
 *
 */
public class SelectQuery {
    private List<String> columns;
    private String table;

    public SelectQuery() {
        this.columns = new ArrayList<String>();
        this.table = "";
    }

    public void setTable(String table) {
        this.table = table;
    }

    public void add(String column) {
        this.columns.add(column);
    }

    public void addAll(String... columns) {
        this.columns.addAll(Arrays.asList(columns));
    }

    public List<String> toList() {
        List<String> columns = new ArrayList<String>();

        for (String column : this.columns) {
            if (!column.contains(".") && !TextUtils.isEmpty(table)) {
                columns.add(table + "." + column);
            } else {
                columns.add(column);
            }
        }

        return columns;
    }

    public String[] toArray() {
        String[] columns = new String[this.columns.size()];
        int i = 0;

        for (String column : this.columns) {
            if (!column.contains(".") && !TextUtils.isEmpty(table)) {
                columns[i] = (table + "." + column);
            } else {
                columns[i] = column;
            }
            i++;
        }

        return columns;
    }

    public int size() {
        return this.columns.size();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        if (columns.size() > 0) {
            for (String column : columns) {
                if (stringBuilder.length() > 0) stringBuilder.append(", ");

                if (!column.contains(".") && !TextUtils.isEmpty(table)) {
                    //Set table prefix if exist
                    stringBuilder.append(this.table).append(".");
                }

                stringBuilder.append(column);
            }
        } else {
            stringBuilder.append("*");
        }

        return stringBuilder.toString();
    }
}
