package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers;

import android.text.TextUtils;

public class JoinOnBuilder {
    private StringBuilder mJoinOn = new StringBuilder();

    public JoinOnBuilder add(boolean and, String table1, String key, String operador, String table2, String value) {
        return add(and, table1 + "." + key, operador, table2 + "." + value);
    }

    public JoinOnBuilder add(boolean and, String key, String value) {
        return add(and,  key, " = ",  value);
    }

    public JoinOnBuilder add(boolean and, String table1, String key, String table2, String value) {
        return add(and, table1 + "." + key, " = ", table2 + "." + value);
    }

    public JoinOnBuilder add(boolean and, String key, String operador, String value) {
        String type = and ? " AND " : " OR ";

        if (!TextUtils.isEmpty(mJoinOn)) mJoinOn.append(type);

        mJoinOn.append(key).append(operador).append(value);
        return this;
    }

    @Override
    public String toString() {
        return mJoinOn.toString();
    }
}
