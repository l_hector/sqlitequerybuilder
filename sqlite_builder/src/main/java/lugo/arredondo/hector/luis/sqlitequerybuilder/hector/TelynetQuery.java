package lugo.arredondo.hector.luis.sqlitequerybuilder.hector;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Locale;

import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers.CursorMapper;
import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers.SelectQuery;
import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers.SimpleWhere;
import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers.StringJoiner;
import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.interfaces.AggregateFuncions;
import lugo.arredondo.hector.luis.sqlitequerybuilder.hector.interfaces.SQLiteFuncions;


/**
 * @author Hector Arredondo on 01/06/2018
 */
public class TelynetQuery {
    private final static String TAG = TelynetQuery.class.getSimpleName();

    private String query;
    private SQLiteDatabase database;
    private Builder builder;

    private TelynetQuery() {
        //private access default construct
    }


    public TelynetQuery(@NotNull SQLiteDatabase database, @NotNull Builder builder) {
        this.builder = builder;
        this.database = database;
        this.query = builder.toString();
    }

    @Nullable
    public Cursor find() throws RuntimeException {
        Cursor cursor = null;

        try {
            cursor = database.rawQuery(query, null);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        finally {
//            database.close();
//        }

        return cursor;
    }

    public <T> T findFirt(Class<T> clazz) {
        try {
            CursorMapper<T> mapper = new CursorMapper<T>(clazz);
            Cursor cursor = find();

            if (cursor != null) {
                return mapper.map(cursor);
            }
        } catch (Exception ex) {
            Log.e(TAG, "findFirt", ex);
        }

        return null;
    }

    public boolean update() {
        boolean isUpdate = false;

        try {
            isUpdate = this.database.update(builder.table, builder.contentValues,
                    builder.whereBuilder.toString(), null) > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            database.close();
        }

        return isUpdate;
    }

    public boolean insertOrUpdate() {
        boolean isUpdate = false;

        try {
            isUpdate = this.database.updateWithOnConflict(builder.table, builder.contentValues,
                    builder.whereBuilder.toString(), null, SQLiteDatabase.CONFLICT_REPLACE) > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            database.close();
        }

        return isUpdate;
    }

    public long insert() {
        long idRow = -1;

        try {
            idRow = this.database.insert(builder.table, null, builder.contentValues);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            database.close();
        }

        return idRow;
    }

    public boolean delete() {
        boolean isDelete = false;

        try {
            isDelete = database.delete(builder.table, builder.whereBuilder.toString(), null) > 0;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            database.close();
        }

        return isDelete;
    }

    public String getRawQuery() {
        return this.query;
    }

    /**
     * Builder creator
     */
    public static class Builder {
        private SelectQuery columns = new SelectQuery();
        private SelectQuery joinColumns = new SelectQuery();
        private String table = "";
        private StringJoiner join = new StringJoiner("\n\t ");
        private StringBuilder whereBuilder = new StringBuilder();
        private boolean distinct = false;
        private String groupBy = null;
        private StringJoiner orderBy = new StringJoiner(", ");
        private String sort = "";
        private String having = null;

        //Update/insert
        private ContentValues contentValues = null;

        public TelynetQuery createReadOnly() throws SQLiteException {
            if (TextUtils.isEmpty(this.table))
                throw new IllegalArgumentException("the table can not be null. Use Builder.table(nomTable) method");

            //TODO DataBase Here
            String myPath = "";//GlobalesDefinicionDatos.DB_PATH + GlobalesDefinicionDatos.DB_NAME;
            SQLiteDatabase database = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

            return new TelynetQuery(database, this);
        }

        public TelynetQuery create(@NotNull SQLiteDatabase database) {
            if (TextUtils.isEmpty(this.table))
                throw new IllegalArgumentException("the table can not be null. Use Builder.table(nomTable) method");

            return new TelynetQuery(database, this);
        }

        private String createQuery() {
            if (TextUtils.isEmpty(groupBy) && !TextUtils.isEmpty(having)) {
                throw new IllegalArgumentException(
                        "HAVING clauses are only permitted when using a groupBy clause");
            }

            StringBuilder queryBuilder = new StringBuilder();

            queryBuilder.append("SELECT ");
            if (distinct) {
                queryBuilder.append("DISTINCT ");
            }

            queryBuilder.append(columns.toString());
            if (joinColumns.size() > 0) queryBuilder.append(joinColumns);
            queryBuilder.append("\n").append(" FROM ").append(table);
            queryBuilder.append(join.toString());

            if (!TextUtils.isEmpty(whereBuilder)) {
                queryBuilder.append("\n ").append(whereBuilder);
            }

            if (!TextUtils.isEmpty(groupBy)) {
                queryBuilder.append("\n ").append(groupBy);
            }
            if (!TextUtils.isEmpty(having)) {
                queryBuilder.append("\n ").append(having);
            }
            if (this.orderBy.size() > 0) {
                queryBuilder.append("\n ").append("ORDER BY ").append(orderBy).append(this.sort);
            }

            return queryBuilder.toString();
        }

        public Builder selectRaw(String selectRaw) {
            this.columns.add(selectRaw);

            return this;
        }

        public Builder selectOnTable(String table, String... columns) {
            for (String column : columns) {
                this.columns.add(table + "." + column);
            }

            return this;
        }

        public Builder select(String... columns) {
            this.columns.addAll(columns);

            return this;
        }

        public Builder selectAs(String column, String as) {
            this.columns.add("( " + column + ") AS " + as);
            return this;
        }

        /**
         * Select column with Aggregate Function
         * {@see https://www.sqlite.org/lang_aggfunc.html#count}
         *
         * @param aggregateFun aggregate function {@link AggregateFuncions}
         * @param column       Column name
         * @return Builder
         */
        public Builder selectWithFunc(@NotNull @SQLiteFuncions String aggregateFun, @NotNull String column) {
            this.select(aggregateFun + "(" + column + ")");
            return this;
        }

        /**
         * Select column with Aggregate Function
         * {@see https://www.sqlite.org/lang_aggfunc.html#count}
         *
         * @param aggregateFun aggregate function {@link AggregateFuncions}
         * @param column       Column name
         * @return Builder
         */
        public Builder selectWithFunc(@NotNull @SQLiteFuncions String aggregateFun, @NotNull String column, String as) {
            this.select(aggregateFun + "(" + column + ") AS " + as);
            return this;
        }

        public Builder joinSelect(String table, String... columns) {
            this.joinColumns.addAll(columns);
            this.joinColumns.setTable(table);

            return this;
        }


        public Builder from(String from) {
            //  this.columns.setTable(from);
            this.table = from;
            return this;
        }

        public Builder table(String table) {
            this.table = table;
            return this;
        }

        public Builder disting() {
            this.distinct = true;
            return this;
        }

        public Builder groupBy(String column) {
            if (!column.contains(".")) {
                this.groupBy = " GROUP BY " + this.table + "." + column;
            } else {
                this.groupBy = " GROUP BY " + column;
            }
            return this;
        }

        public Builder groupBy(String table, String column) {
            this.groupBy = " GROUP BY " + table + "." + column;
            return this;
        }

        public Builder orderBy(String... columns) {
            for (String column : columns) {
                this.orderBy.add(column);
            }

            return this;
        }

        public Builder ascending() {
            this.sort = " asc";
            return this;
        }

        public Builder descending() {
            this.sort = " desc";
            return this;
        }

        public Builder innerJoin(String table, SimpleWhere on) {
            this.join.add(" INNER JOIN " + table + " ON " + on.toString() + " ");
            return this;
        }

        public Builder innerJoin(String table, String on) {
            this.join.add(" INNER JOIN " + table + " ON " + on + " ");
            return this;
        }

        public Builder leftJoin(String table, SimpleWhere on) {
            this.join.add(" LEFT JOIN " + table + " ON " + on.toString() + " ");
            return this;
        }

        public Builder leftJoin(String table, String on) {
            this.join.add(" LEFT JOIN " + table + " ON " + on + " ");
            return this;
        }

        public Builder rightJoin(String table, SimpleWhere on) {
            this.join.add(" RIGHT JOIN " + table + " ON " + on.toString() + " ");
            return this;
        }

        public Builder rightJoin(String table, String on) {
            this.join.add(" RIGHT JOIN " + table + " ON " + on + " ");
            return this;
        }

        //update
        public Builder update(@NotNull String[] keys, @NotNull String[] values) {
            if (keys.length != values.length)
                throw new IllegalArgumentException(values.length + " values for " + keys.length + " columns from " + this.table);

            if (contentValues == null) contentValues = new ContentValues();

            for (int i = 0; i < keys.length; i++) {
                contentValues.put(keys[i], values[i]);
            }

            return this;
        }

        public Builder update(String key, String value) {
            if (contentValues == null) contentValues = new ContentValues();

            contentValues.put(key, value);
            return this;
        }

        //where
        public Builder whereRaw(String whereRaw) {
            return where(true, whereRaw, "", "");
        }

        public Builder whereRaw(SimpleWhere whereRaw) {
            return where(true, whereRaw.toString(), "", "");
        }

        public Builder orWhereRaw(String whereRaw) {
            return where(false, whereRaw, "", "");
        }

        public Builder likeWhere(String key, String value) {
            return where(true, key, "LIKE", value);
        }

        public Builder orLikeWhere(String key, String value) {
            return where(false, key, "LIKE", value);
        }

        public <T extends Number> Builder where(String key, String operator, T value) {
            return where(true, key, operator, value);
        }

        public <T extends String> Builder where(String table, String key, String operator, T value) {
            return where(true, table + "." + key, operator, value);
        }

        public <T extends String> Builder where(String key, String operator, T value) {
            return where(true, key, operator, value);
        }

        public <T extends Number> Builder orWhere(String key, String operator, T value) {
            return where(false, key, operator, value);
        }

        public <T extends String> Builder orWhere(String key, String operator, T value) {
            return where(false, key, operator, value);
        }

        public <T extends String> Builder whereIn(String key, T... values) {
            return createWhereIn(true, key, values);
        }

        public <T extends Number> Builder whereIn(String key, T... values) {
            return createWhereIn(true, key, values);
        }

        public <T extends String> Builder orWhereIn(String key, T... values) {
            return createWhereIn(false, key, values);
        }

        public <T extends Number> Builder orWhereIn(String key, T... values) {
            return createWhereIn(false, key, values);
        }

        /**
         * Add where condition integer values key = value | key > value
         * If set <b>from<b/> table in query no required TABLET.COLUMN format, set auto {@link this.from()}
         *
         * @param and      <b>true</b> if set AND else <b>false</b> set OR
         * @param operator where operator example: =, like, <, >
         * @param key      column key value
         * @param value    where condition value
         * @param <T>      restrict only Number T extends
         * @return TelynetBuilder
         */
        private <T extends Number> Builder where(boolean and, String key, String operator, T value) {
            this.addWhere(and, key, operator, String.valueOf(value));

            return this;
        }

        /**
         * Add where condition Text values  key = 'value' | key LIKE '%value%'
         * If set <b>from<b/> table in query no required TABLET.COLUMN format, set auto {@link this.from()}
         *
         * @param and      <b>true</b> if set AND else <b>false</b> set OR
         * @param operator where operator example: =, like, <, >
         * @param key      column key value
         * @param value    where condition value
         * @param <T>      restrict only String T extends
         * @return TelynetBuilder
         */
        private <T extends String> Builder where(boolean and, String key, String operator, T value) {
            String sValue = "";

            if (!TextUtils.isEmpty(operator)) {
                if (operator.trim().toUpperCase().equals("LIKE")) {
                    sValue = "'%" + value + "%'";
                } else if (!value.contains(".")) {
                    //Si value contiene punto no es un valor, es un atributo de una tabla
                    sValue = "'" + value + "'";
                } else {
                    sValue = value;
                }
            } else {
                //TODO where here raw value
            }

            this.addWhere(and, key, operator, sValue);
            return this;
        }

        public Builder whereNull(String column) {
            addWhere(true, column, " IS NULL ", "");
            return this;
        }

        public Builder orWhereNull(String column) {
            addWhere(false, column, " IS NULL ", "");
            return this;
        }

        public Builder whereNotNull(String column) {
            addWhere(true, column, " IS NOT NULL ", "");
            return this;
        }

        public Builder orWhereNotNull(String column) {
            addWhere(true, column, " IS NOT NULL ", "");
            return this;
        }

        /*  Private method  */
        private <T> Builder createWhereIn(boolean and, String key, T... values) {
            StringJoiner joiner = new StringJoiner(",");

            for (T value : values) {
                if (value instanceof String) {
                    joiner.Add("'" + value + "'");
                } else {
                    joiner.Add(value.toString());
                }
            }

            return where(and, key, "IN", "(" + joiner.toString() + ")");
        }

        private void addWhere(boolean and, String key, String operator, String value) {
            String sWhere;
            String whereType = and ? "AND" : "OR";

            if (!TextUtils.isEmpty(whereBuilder)) {
                sWhere = String.format(Locale.getDefault(), " %1$s %2$s %3$s %4$s", whereType, key, operator, value);
            } else {
                sWhere = String.format(Locale.getDefault(), " WHERE %1$s %2$s %3$s", key, operator, value);
            }

            this.whereBuilder.append(sWhere);
        }

        public String toStringCompac() {
            return " (" + createQuery() + ") ";
        }

        @Override
        public String toString() {
            return createQuery();
        }
    }
}