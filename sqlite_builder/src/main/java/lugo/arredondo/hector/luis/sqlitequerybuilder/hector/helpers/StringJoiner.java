package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hector Arredondo on 01/06/2018
 *
 */
public class StringJoiner {
    private List<String> joins;
    private String separator;

    private StringJoiner() { }

    public StringJoiner(String separator) {
        this.separator = separator;
        this.joins = new ArrayList<String>();
    }

    public void add(String join) {
        this.joins.add(join);
    }

    public StringJoiner Add(String join) {
        this.joins.add(join);
        return this;
    }

    public int size() {
        return joins.size();
    }

    @Override
    public String toString() {
        StringBuilder sJoin = new StringBuilder();

        for (String join : joins){
            if (!TextUtils.isEmpty(sJoin) || separator.contains("\n\t") || separator.equals(" ")) sJoin.append(this.separator);

            sJoin.append(join);
        }

        return sJoin.toString();
    }
}
