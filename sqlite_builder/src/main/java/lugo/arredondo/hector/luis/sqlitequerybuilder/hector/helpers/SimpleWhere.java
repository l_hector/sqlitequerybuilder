package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers;

import android.text.TextUtils;

import java.util.Locale;

public class SimpleWhere {
    private StringBuilder whereBuilder;
    private boolean removeWhere = false;

    public SimpleWhere(boolean removeWhere) {
        this.whereBuilder = new StringBuilder();
        this.removeWhere = removeWhere;
    }

    public SimpleWhere() {
        this.whereBuilder = new StringBuilder();
    }

    public void removeWhere() {
        this.removeWhere = true;
    }

    public void whereRaw(String whereRaw) {
        addWhere(true, whereRaw, "", "");
    }

    public void orWhereRaw(String whereRaw) {
        addWhere(false, whereRaw, "", "");
    }

    public void whereIn(String key, String... values) {
        createWhereIn(true, key, values);
    }

    public void orWhereIn(String key, String... values) {
        createWhereIn(false, key, values);
    }

    public <T extends String> void orWhere(String key, String operator, T value) {
        String sValue;

        if (operator.trim().toUpperCase().equals("LIKE")) {
            sValue = "'%" + value + "%'";
        } else {
            sValue = "'" + value + "'";
        }

        addWhere(false, key, operator, sValue);
    }

    public <T extends String> void where(String key, String operator, T value) {
        String sValue = value;

        if (operator.trim().toUpperCase().equals("LIKE")) {
            sValue = "'%" + value + "%'";
        } else if (!value.contains(".")){
            //Si con tiene un punto no es un valor, es un atributo de una tabla
            sValue = "'" + value + "'";
        }

        addWhere(true, key, operator, sValue);
    }

    private void createWhereIn(boolean and, String key, String... values) {
        StringJoiner joiner = new StringJoiner(",");

        for (String value : values) {
            joiner.Add("'" + value + "'" );
        }

        addWhere(and, key, "IN", "(" + joiner.toString() + ")");
    }

    public void whereNull(String column) {
        addWhere(true, column, " IS NULL ", "");
    }

    public void orWhereNull(String column) {
        addWhere(false, column, " IS NULL ", "");
    }

    public void whereNotNull(String column) {
        addWhere(true, column, " IS NOT NULL ", "");
    }

    public void orWhereNotNull(String column) {
        addWhere(false, column, " IS NOT NULL ", "");
    }

    private void addWhere(boolean and, String key, String operator, String value) {
        String sWhere;
        String whereType = and ? "AND" : "OR";

        if (TextUtils.isEmpty(whereBuilder)) {
            sWhere = String.format(Locale.getDefault(), " %1$s %2$s %3$s", key, operator, value);
        } else {
//            sWhere = String.format(Locale.getDefault(), " WHERE %1$s %2$s %3$s", key, operator, value);
            sWhere = String.format(Locale.getDefault(), " %1$s %2$s %3$s %4$s", whereType, key, operator, value);
        }

        whereBuilder.append(sWhere);
    }

    public String toStringCompact() {
        return " ( " + whereBuilder.toString() + " ) ";
    }

    @Override
    public String toString() {
        String sWhere = whereBuilder.toString();

//        if (removeWhere) {
//            sWhere = sWhere.replace("WHERE", "");
//        }
        return sWhere;
    }
}
