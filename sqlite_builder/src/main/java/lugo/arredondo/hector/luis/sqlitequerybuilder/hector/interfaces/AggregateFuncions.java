package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.interfaces;

/**
 * @author H?ctor Arredondo on 01/06/2018
 *
 * Aggregate Functions interfaces
 */
public interface AggregateFuncions {
    /**
     * <b>avg(X)</b>
     * The avg() function returns the average value of all non-NULL X within a group. String and BLOB values that do not look like numbers are interpreted as 0. The result of avg() is always a floating point value as long as at there is at least one non-NULL input even if all inputs are integers. The result of avg() is NULL if and only if there are no non-NULL inputs.
     */
    String AVG = "AVG";
    /**
     * <b>count(X)</b>
     * <b>count(*)</b>
     * The count(X) function returns a count of the number of times that X is not NULL in a group. The count(*) function (with no arguments) returns the total number of rows in the group.
     */
    String COUNT = "COUNT";
    /**
     * <b>group_concat(X)</b>
     * <b>group_concat(X,Y)</b>
     * The group_concat() function returns a string which is the concatenation of all non-NULL values of X. If parameter Y is present then it is used as the separator between instances of X. A comma (",") is used as the separator if Y is omitted. The order of the concatenated elements is arbitrary.
     */
    String GROUP_CONCAT = "group_concat";
    /**
     * <b>max(X)</b>
     * The max() aggregate function returns the maximum value of all values in the group. The maximum value is the value that would be returned last in an ORDER BY on the same column. Aggregate max() returns NULL if and only if there are no non-NULL values in the group.
     */
    String MAX = "MAX";
    /**
     * <b>min(X)</b>
     * The min() aggregate function returns the minimum non-NULL value of all values in the group. The minimum value is the first non-NULL value that would appear in an ORDER BY of the column. Aggregate min() returns NULL if and only if there are no non-NULL values in the group.
     */
    String MIN = "MIN";
    /**
     * <b>sum(X)</b>
     * <b>total(X)</b>
     *
     * The sum() and total() aggregate functions return sum of all non-NULL values in the group. If there are no non-NULL input rows then sum() returns NULL but total() returns 0.0. NULL is not normally a helpful result for the sum of no rows but the SQL standard requires it and most other SQL database engines implement sum() that way so SQLite does it in the same way in order to be compatible. The non-standard total() function is provided as a convenient way to work around this design problem in the SQL language.
     *
     * The result of total() is always a floating point value. The result of sum() is an integer value if all non-NULL inputs are integers. If any input to sum() is neither an integer or a NULL then sum() returns a floating point value which might be an approximation to the true sum.
     *
     * Sum() will throw an "integer overflow" exception if all inputs are integers or NULL and an integer overflow occurs at any point during the computation. Total() never throws an integer overflow.
     */
    String SUM = "SUM";
    String TOTAL = "total";
}
