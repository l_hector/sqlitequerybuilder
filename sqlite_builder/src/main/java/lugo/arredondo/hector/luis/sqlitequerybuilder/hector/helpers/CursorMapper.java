package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.helpers;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class CursorMapper<T> {
    private final static String TAG = CursorMapper.class.getSimpleName();

    private Class<T> mClazz;
    private T data;
    private List<MapField> mapFields;

    private CursorMapper() {
    }

    public CursorMapper(Class<T> clazz) {
        this.mClazz = clazz;
        this.mapFields = new ArrayList<MapField>();
    }

    public T map(@NotNull Cursor cursor) {
        try {
            this.data = this.mClazz.newInstance();

            initFields();

            if (cursor.moveToFirst()) {
                do {
                    startMapper(cursor);
                } while (cursor.moveToFirst());
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return data;
    }

    private void initFields() {
        Field[] fields = this.mClazz.getFields();

        for (Field field : fields) {
            String prop = field.getName();
            Class clazz = field.getType();

            this.mapFields.add(new MapField(clazz, prop));
        }
    }

    private void startMapper(Cursor cursor) {
        for (MapField mapField : mapFields) {
            String propPascalCase = toPascalCase(mapField.prop);

            if (hasColumn(cursor, propPascalCase)) {
                try {
                    getCursorValue(cursor, mapField.clazz, propPascalCase, mapField.prop);
                } catch (ClassNotFoundException e) {
                    Log.e(TAG, "startMapper", e);
                } catch (NoSuchMethodException e) {
                    Log.e(TAG, "startMapper prop " + mapField.prop + " not found", e);
                } catch (IllegalAccessException e) {
                    Log.e(TAG, "startMapper", e);
                } catch (InvocationTargetException e) {
                    Log.e(TAG, "startMapper", e);
                }
            }
        }
    }

    /**
     * Change camel case to pascal case
     *
     * @param value String
     * @return if miVariable is parameter result <b>mi_variable</b>
     */
    private String toPascalCase(String value) {
        if (value.contains("_")) {
            //no convert to pascal case
            return value;
        }


        String[] aValues = value.split("(?=\\p{Upper})");
        StringBuilder stringBuilder = new StringBuilder();


        for (String s : aValues) {
            if (!TextUtils.isEmpty(stringBuilder)) stringBuilder.append("_");

            stringBuilder.append(s.toLowerCase());
        }

        return stringBuilder.toString();
    }

    /**
     * Check if column name exist in current cursor
     *
     * @param cursor Current cursor
     * @param column Column name
     * @return <b>true</b> if exist <b>false</b> if not exit
     */
    private boolean hasColumn(Cursor cursor, String column) {
        return cursor.getColumnIndex(column) > -1;
    }

    /**
     * Get value from cursor
     *
     * @param cursor     Current cursor
     * @param clazz      Class<T> type
     * @param propPascal Property name in pascal case
     * @param prop       Property name in camel case
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    private void getCursorValue(Cursor cursor, Object clazz, String propPascal, String prop) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Object value = null;

        if (clazz instanceof Integer) {
            value = cursor.getInt(cursor.getColumnIndex(prop));
        } else if (clazz instanceof Boolean) {
            value = cursor.getBlob(cursor.getColumnIndex(prop));
        } else if (clazz instanceof String) {
            value = cursor.getString(cursor.getColumnIndex(prop));
        } else if (clazz instanceof Float) {
            value = cursor.getFloat(cursor.getColumnIndex(prop));
        } else if (clazz instanceof Double) {
            value = cursor.getDouble(cursor.getColumnIndex(prop));
        } else if (clazz instanceof Long) {
            value = cursor.getLong(cursor.getColumnIndex(prop));
        } else {
            Log.e(TAG, "prop: " + prop + " type: " + clazz.getClass().getSimpleName() + " not found in cursor");
        }

        if (value != null) updateField(prop, value);
    }

    //Private method
    private void updateField(String prop, Object value) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException {
        Class typeClass = getClazz(value);

        if (typeClass.equals(boolean.class)) {
            //case special, if prop name "isValue" Setter method are "setValue"
            String is = prop.substring(0, 2);
            if (is.equals("is")) prop = prop.replace("is", "");
        }

        String chart = prop.substring(0, 1);
        String sMethod = "set";


        sMethod += chart.toUpperCase() + prop.substring(1, prop.length());

        Method method = mClazz.getMethod(sMethod, typeClass);
        method.invoke(data, value);
    }

    /**
     * getClass from instance Class
     *
     * @param clazz to instance
     * @return Class
     * @throws ClassNotFoundException
     */
    private Class getClazz(Object clazz) throws ClassNotFoundException {
        if (clazz instanceof Integer) return int.class;
        else if (clazz instanceof Boolean) return boolean.class;
        else if (clazz instanceof String) return String.class;
        else if (clazz instanceof Float) return float.class;
        else if (clazz instanceof Double) return double.class;
        else if (clazz instanceof Long) return long.class;

        throw new ClassNotFoundException();
    }

    private class MapField {
        Class clazz;
        String prop;

        private MapField() {

        }

        public MapField(Class clazz, String prop) {
            this.clazz = clazz;
        }
    }
}
