package lugo.arredondo.hector.luis.sqlitequerybuilder.hector.interfaces;

@StringDef({AggregateFuncions.AVG, AggregateFuncions.COUNT, AggregateFuncions.GROUP_CONCAT,
        AggregateFuncions.MAX, AggregateFuncions.MIN, AggregateFuncions.SUM, AggregateFuncions.TOTAL})
public @interface SQLiteFuncions {}